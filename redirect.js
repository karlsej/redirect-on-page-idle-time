/* globals */
var returnWarning;
var redirectToSearch;
var functionActive = false;
var listenerAttached = false;

function redirectWarn() {
  if (functionActive === false) {
    console.log('I\'m running');
    functionActive = true;
    returnWarning = setTimeout(function() {
      var body = document.getElementsByTagName('body')[0];
      var warningDiv = document.createElement('div');
      warningDiv.id = 'redirect-warning';
      warningDiv.innerHTML = '<p>Redirecting to Library Search Page.</p><p>Not done yet?</p><div><button type="button" class="button" id="stay-on-page" onclick="clearWarning();">Stay on this page</button></div>';
      body.appendChild(warningDiv);
      sccSearchRedirect(); // call the function to redirect to website

    }, 300000);
    //        }, 100000); // for testing
  }
}

function clearWarning() {
  clearTimeout(redirectToSearch); // so clicking button cancels the redirect
  clearTimeout(returnWarning); // it also cancels any other existing timeouts if they have been created? confusing
  // remove the div we've created from the dom
  var item = document.getElementById('redirect-warning');
  item.parentNode.removeChild(item);
  functionActive = false; // set this flag to false so that the function can run again.
}


function sccSearchRedirect() {
  redirectToSearch = setTimeout(function() {
    window.location.href = 'http://www.scc.losrios.edu/library/library-catalog-search';
  }, 30000);
  setTimeout(function() { // extra warning at 5 seconds to go
    var redirectNow = document.createElement('div');
    redirectNow.id = 'redirecting-now';
    redirectNow.innerHTML = '<p>Redirecting now...</p>';
    var warning = document.getElementById('redirect-warning');
    if (warning) {
      warning.appendChild(redirectNow);
    }
  }, 25000);

}

setInterval(function() { // starts the function of clearing the timeouts -- but do we need it to be on setInterval, or is once enough?
  if (listenerAttached === false) { // careful not to attach multiple listeners
    window.addEventListener('mousemove', function() {
      clearTimeout(returnWarning); // cancels window from appearing
      functionActive = false; // not sure about this one!
      listenerAttached = true; // so this does not run again
      //       redirectWarn();
    });
  }
}, 2000);


setInterval(function() {
  if (functionActive === false) {
    redirectWarn();
  }
}, 30000);